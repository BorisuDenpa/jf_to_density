# Json to density

![Image 1](sample.jpg)

![Sample density](sample2.jpg)

## to do:
- Verify that the points aren't out of bounds 
- Optimize speed

For some weird reason, the points in the jsons or h5 files from the UCF_CC_50  
dataset have lots of decimals, which are irrelevant, starting from the fact that  
those points where entered by a person, probably with a mouse, more precision than  
a pixel is overkill 