import cv2
from matplotlib import pyplot as plt
import numpy as np
import scipy
from scipy import spatial,ndimage
from scipy.ndimage.filters import gaussian_filter
import os
import sys
import json
import math
import time
import random
from random import shuffle
import pickle
import h5py
import glob
import math
import gc
gc.enable()

# Path in which to look for images and json files
dataset_paths = ['data']


# 1
# Loads and checks for images as jpg or png, and jsons
def load_images_and_gts(path):
    for gt_file in glob.glob(os.path.join(path, '*.json')):
        print(gt_file)
        if os.path.isfile(gt_file.replace('.json', '.png')):
            img = cv2.imread(gt_file.replace('.json', '.png'), cv2.IMREAD_GRAYSCALE)
        else:
            img = cv2.imread(gt_file.replace('.json', '.jpg'), cv2.IMREAD_GRAYSCALE)
        head, tail = os.path.split(gt_file)
        name = os.path.splitext(tail)[0]
        # Saves the grayscale converted images to the output path
        cv2.imwrite('samples/'+name+'.jpg',img)
        # load ground truth
        gt = load_gt_from_json(gt_file, img.shape, name)
    print(path, 'loaded')
    return


# 2
# Loads data from the json files, points pointing where people is in the images
# and generates a np.array which is the density map for the given image
def load_gt_from_json(gt_file, gt_shape,name):
    gt = np.zeros(gt_shape, dtype='uint8')
    with open(gt_file, 'r') as jf:
        pts = []
        # Loads every point and gets its int value
        for k, pt in enumerate(json.load(jf)):
            pts.append([math.trunc(pt['y']), math.trunc(pt['x'])])
        # Does a search for the closest neighbor between the points, getting the
        # closest point (unused) and the distance to that point
        leafsize = 2048
        tree = scipy.spatial.KDTree(pts.copy(), leafsize=leafsize)
        distances, locations = tree.query(pts, k=2, eps=10.)
        distances = distances[:,1]
        # Gets the density map
        for j, dot in enumerate(pts):
            print(j)
            gt = densidad(pts[j][0], pts[j][1], gt, gt_shape,distances,j)
    # h5f = h5py.File('data/'+ name+ '.h5', 'x')
    # h5f.create_dataset('dataset', data=gt)
    # Saves every density map as a npy file
    np.save('samples/'+ name+ '.npy', gt)
    # Uncomment to get the total value of people that'd be in the density map
    #print(np.sum(gt))
    #print("numpysum")
    # Uncomment to show a normalized/clipped image representation of the density map
    #gt = 255*(gt/gt.max())
    #cv2.imwrite('asdf.jpg',gt)
    #cv2.imshow('image',gt)
    #cv2.waitKey(10)
    return gt

# 3
# Gets the density map
def densidad(x, y, density, gt_shape, distances,j,d=0):
    last = np.zeros(gt_shape)
    # Just for the UCF database
    if x>=gt_shape[0]:
        x=gt_shape[0]-1
    if y>=gt_shape[1]:
        y=gt_shape[1]-1
    # Gives value 1 to every point in the json file
    last[x][y] = 1
    # Calls to the blur def
    last = blur(last,distances,j)
    # Adds the blurred point to the density map for the given image
    density = np.add(density, last)
    return density

# 4
# Applies a gaussian filter to every point keeping the total value for every point
# as 1, using as sigma the distance to the another closest point, this means for
# more sparse points more diffumination, less dense. 
def blur(toblur,distances,j):
    sigma = distances[j]
    blurred = gaussian_filter(toblur, sigma)
    return blurred



for path in dataset_paths:
    load_images_and_gts(path)

print("Finished")
cv2.waitKey(0)
cv2.destroyAllWindows()
